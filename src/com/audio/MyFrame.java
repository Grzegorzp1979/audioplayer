package com.audio;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.awt.*;
import java.io.File;

public class MyFrame extends JFrame {

    JProgressBar progressBar;

    JMenuBar menuBar;

    JMenu fileMenu;

    JMenuItem openItem;
    JMenuItem exitItem;

    JFileChooser fileChooser;

    JLabel label = new JLabel();
    JPanel panel = new JPanel();
    JPanel panel1 = new JPanel();

    JButton buttonPlay = new JButton();
    JButton buttonStop = new JButton();
    JButton buttonReset = new JButton();

    ImageIcon icon = new ImageIcon("homer.png");
    ImageIcon icon2 = new ImageIcon("musical_notes.png");
    ImageIcon icon3 = new ImageIcon("opened_folder.png");
    ImageIcon icon4 = new ImageIcon("exit.png");

    String[] messages = {"Yes please", "No thanks"};

    File file;
    Clip clip;
    long stopMusic;

    MyFrame() {

        buttonPlay.setBounds(0, 0, 200, 75);
        buttonPlay.addActionListener(e -> {
            if (e.getSource() == openItem) {
                fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("C:\\Users\\pc\\Desktop\\Java\\Music"));
                fileChooser.showOpenDialog(null);
            }
            if (e.getSource() == buttonPlay) {
                if (stopMusic == 0) {
                    try {
                        file = new File(fileChooser.getSelectedFile().getAbsolutePath());
                        AudioInputStream audioFile = AudioSystem.getAudioInputStream(file);
                        clip = AudioSystem.getClip();
                        clip.open(audioFile);
                        clip.start();
                    } catch (Exception e1) {
                        e1.setStackTrace(null);
                    }
                } else {
                    clip.setMicrosecondPosition(stopMusic);
                    clip.start();
                }
                label.setText("NOW MUSIC IS PLAYING");
            }
        });
        buttonPlay.setText("PLAY");
        buttonPlay.setBorder(BorderFactory.createEtchedBorder());
        buttonPlay.setForeground(Color.BLACK);
        buttonPlay.setFocusable(false);
        buttonPlay.setFont(new Font("Arial", Font.ITALIC, 25));

        buttonStop.setBounds(0, 75, 200, 75);
        buttonStop.addActionListener(e -> {
            if (e.getSource() == buttonStop) {
                clip.stop();
                stopMusic = clip.getMicrosecondPosition();
                System.out.println(stopMusic);
                label.setText("MUSIC STOP");
            }
        });
        buttonStop.setText("STOP");
        buttonStop.setBorder(BorderFactory.createEtchedBorder());
        buttonStop.setForeground(Color.BLACK);
        buttonStop.setFocusable(false);
        buttonStop.setFont(new Font("Arial", Font.ITALIC, 25));

        buttonReset.setBounds(0, 150, 200, 75);
        buttonReset.addActionListener(e -> {
            if (e.getSource() == buttonReset) {
                clip.setMicrosecondPosition(0);
            }
        });
        buttonReset.setText("RESET");
        buttonReset.setBorder(BorderFactory.createEtchedBorder());
        buttonReset.setForeground(Color.BLACK);
        buttonReset.setFocusable(false);
        buttonReset.setFont(new Font("Arial", Font.ITALIC, 25));

        panel.setBounds(0, 0, 200, 225);
        panel.setBackground(new Color(0x719e79));
        panel.setLayout(null);
        panel.add(buttonPlay);
        panel.add(buttonStop);
        panel.add(buttonReset);

        label.setBounds(0, 0, 300, 150);
        label.setFont(new Font("Broadway", Font.BOLD, 20));
        label.setForeground(Color.BLACK);
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);

        progressBar = new JProgressBar();
        progressBar.setValue(0);
        progressBar.setBounds(150, 50, 400, 50);
        progressBar.setStringPainted(true);
        progressBar.setFont(new Font("MV Boli", Font.PLAIN, 25));

        panel1.setBounds(200, 0, 400, 225);
        panel1.setLayout(new BorderLayout());
        panel1.setBackground(new Color(0x67a2d2));
        panel1.add(label);
        //panel1.add(progressBar);

        menuBar = new JMenuBar();

        fileMenu = new JMenu("File");

        openItem = new JMenuItem("Open");
        openItem.setIcon(icon3);
        openItem.addActionListener(e -> {
            if (e.getSource() == openItem) {
                fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("C:\\Users\\pc\\Desktop\\Java\\Music"));
                fileChooser.showOpenDialog(null);
            }
        });

        exitItem = new JMenuItem("Exit");
        exitItem.setIcon(icon4);
        exitItem.addActionListener(e -> {
            if (e.getSource() == exitItem) {
                int value = JOptionPane.showInternalOptionDialog(
                        null,
                        "Do you wat to quit?",
                        "Quit Player",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        icon,
                        messages,
                        0);
                if (value == 0) {
                    System.out.println("Bye Bye");
                    System.exit(0);
                }
            }
        });

        fileMenu.add(openItem);
        fileMenu.add(exitItem);
        menuBar.add(fileMenu);

        this.setSize(600, 285);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setIconImage(icon2.getImage());
        this.setTitle("Greg Player");
        this.add(panel);
        this.add(panel1);
        this.setJMenuBar(menuBar);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
